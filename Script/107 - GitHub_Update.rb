# encoding: utf-8
#==============================================================================
# ■ GitHub_Update
#------------------------------------------------------------------------------
# 　GitHub 自动更新。
#   Author: guoxiaomi
#==============================================================================

module GitHub_Update
  Owner = "qq634488405"
  Repo = "RMXP-Gmud"
  Version = "V1.14.20240111"
end

module GitHub_Update
  # 新建下载文件夹
  Folder = "Update"
  if !FileTest.exist?("#{Folder}/dl")
    Dir.mkdir("#{Folder}/dl")
  end
  # 定义网络及本地路径
  Release_Url = "https://api.github.com/repos/#{Owner}/#{Repo}/releases/latest"
  Patch_Url = "https://github.com/#{Owner}/#{Repo}/compare/__OLD__...__NEW__.patch"
  Release_Path = "#{Folder}/dl/latest_release.json"
  Patch_Path = "#{Folder}/dl/__OLD__...__NEW__.patch"
  # API下载
  URLDownloadToFile = Win32API.new("Urlmon", "URLDownloadToFile", "ippii", "i")
  module_function
  #--------------------------------------------------------------------------
  # ● 获取当前版本及最新版本
  #--------------------------------------------------------------------------
  def version
    @old_tag = Version
    begin
      URLDownloadToFile.call(0, Release_Url, Release_Path, 0, 0)
      @new_tag = File.read(Release_Path).scan(
        /"tag_name":\s*"(.+?)"/
      )[0][0]
    rescue
      @new_tag = @old_tag
    end
    return @old_tag, @new_tag
  end
  #--------------------------------------------------------------------------
  # ● 更新处理
  #--------------------------------------------------------------------------
  def update
    return if @old_tag == @new_tag
    File.open("#{Folder}/dl/update.cmd", "w") do |f|
      f.puts update_cmd
    end
    exec("start #{Folder}\\dl\\update.cmd")
  end
  #--------------------------------------------------------------------------
  # ● 写入CMD
  #--------------------------------------------------------------------------
  def update_cmd
    cmd = ""
    cmd += "set PATH=.\\#{Folder};%PATH%\n"
    cmd += "aria2c --allow-overwrite -o #{Patch_Path} #{Patch_Url}\n"
    cmd += "git-apply #{Patch_Path}\n"
    cmd += "start Gmud.exe\n"
    cmd += "exit\n"
    return cmd.gsub("__OLD__", @old_tag).gsub("__NEW__", @new_tag)
  end
end